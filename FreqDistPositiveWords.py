import csv
import nltk
import numpy as np
from nltk import FreqDist
from nltk.tokenize import word_tokenize 
import csv

positive = open('positiveSentence.csv', 'r')

#Join All tokens into one string
text = " ".join(x.strip('\n') for x in positive)

#Get the frequency distribution for each word
tokens = nltk.word_tokenize(text)
fdist=FreqDist(tokens)

#Get a list of the 50 most frequently occurring types in the text
for word, frequency in fdist.most_common(30):
	print(u'{} : {}'.format(word, frequency))
