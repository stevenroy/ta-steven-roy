import csv
import nltk
import numpy as np
from nltk import FreqDist
from nltk.tokenize import word_tokenize 
import csv
import matplotlib.pyplot as plt

negative = open('negativeSentence.csv', 'r')

#Join All tokens into one string
text = " ".join(x.strip('\n') for x in negative)

#Get the frequency distribution for each word
tokens = nltk.word_tokenize(text)
fdist=FreqDist(tokens)

fdist.plot(30)
